package com.yoga.movieapp.utils

fun Any.getClassName() = this::class.java.simpleName