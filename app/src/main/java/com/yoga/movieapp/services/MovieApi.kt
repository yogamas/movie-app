package com.yoga.movieapp.services

import com.yoga.movieapp.BuildConfig
import com.yoga.movieapp.services.network.apiadapter.ApiResponse
import com.yoga.movieapp.model.BaseModel
import com.yoga.movieapp.model.MovieDetailItem
import com.yoga.movieapp.model.Results
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface MovieApi {
    @GET("movie/now_playing")
    suspend fun getMovieList(@Query("limit")limit: String, @Query("api_key") apiKey: String = BuildConfig.API_KEY): ApiResponse<BaseModel<List<Results>>>

    @GET("movie/{id}")
    suspend fun detailMovie(@Path("id") id:Int, @Query("limit")limit: String?=null, @Query("api_key") apiKey: String = BuildConfig.API_KEY): ApiResponse<MovieDetailItem>


}