package com.yoga.movieapp.services.network

import com.google.gson.Gson
import com.yoga.movieapp.BuildConfig
import com.yoga.movieapp.services.network.apiadapter.ApiResponseCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class BaseResponse {
    val httpLogging = HttpLoggingInterceptor()

    private fun buildRetrofit(): Retrofit{
        httpLogging.level = HttpLoggingInterceptor.Level.BODY
        val client = OkHttpClient.Builder().addInterceptor(httpLogging).build()
        return Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(ApiResponseCallAdapterFactory())
            .build()
    }

    fun <T> createServices(services: Class<T>): T{
        return buildRetrofit().create(services)
    }
}