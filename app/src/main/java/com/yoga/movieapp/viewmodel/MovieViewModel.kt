package com.yoga.movieapp.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.yoga.movieapp.model.MovieDetailItem
import com.yoga.movieapp.model.Results
import com.yoga.movieapp.services.MovieApi
import com.yoga.movieapp.services.network.BaseResponse
import com.yoga.movieapp.services.network.apiadapter.ApiErrorResponse
import com.yoga.movieapp.services.network.apiadapter.ApiSuccessResponse
import com.yoga.movieapp.utils.getClassName
import kotlinx.coroutines.launch
import java.lang.Exception

class MovieViewModel : ViewModel() {
    var listMovie = MutableLiveData<List<Results>>()
    var detailMovie = MutableLiveData<MovieDetailItem>()
    fun getList(limit: String) = viewModelScope.launch {
        when(val apiResponse = BaseResponse().createServices(MovieApi::class.java).getMovieList(limit)){
            is ApiSuccessResponse ->{
                listMovie.postValue(apiResponse.body.results)
            }
            is ApiErrorResponse -> {
                Log.e(getClassName(), "getList: ${apiResponse.errorMessage}", )
            }
        }

    }

    fun detailMovie(id: Int) = viewModelScope.launch {
        when(val apiResponse = BaseResponse().createServices(MovieApi::class.java).detailMovie(id)){
            is ApiSuccessResponse ->{
                detailMovie.postValue(apiResponse.body)
            }
            is ApiErrorResponse -> {
                Log.e(getClassName(), "getNowShowing: ${apiResponse.errorMessage}")
            }
        }
    }
}