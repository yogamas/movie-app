package com.yoga.movieapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.yoga.movieapp.adapter.MovieAdapter
import com.yoga.movieapp.databinding.ActivityMainBinding
import com.yoga.movieapp.model.Results
import com.yoga.movieapp.utils.getClassName
import com.yoga.movieapp.viewmodel.MovieViewModel
import java.text.SimpleDateFormat
import java.util.*

class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var movieViewModel: MovieViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        movieViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(MovieViewModel::class.java)
        movieViewModel.getList("3")
        showLoading(true)
        movieViewModel.listMovie.observe(this, { data->
            if (data.isNullOrEmpty()){
                Toast.makeText(this, "Something Wrong", Toast.LENGTH_SHORT).show()
            } else {
                showLoading(false)
                showMovie(data)
            }

        })
    }

    private fun showMovie(list: List<Results>){
        binding.rvMovie.apply {
            adapter = MovieAdapter(this@HomeActivity, list)
            layoutManager = LinearLayoutManager(this@HomeActivity)
        }
    }

    private fun showLoading(isLoading: Boolean){
        if(isLoading){
            binding.pbMovie.visibility = View.VISIBLE
        } else {
            binding.pbMovie.visibility = View.GONE
        }
    }
}