package com.yoga.movieapp.ui

import android.content.Context
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.yoga.movieapp.BuildConfig
import com.yoga.movieapp.R
import com.yoga.movieapp.databinding.ActivityDetailMovieBinding
import com.yoga.movieapp.model.MovieDetailItem
import com.yoga.movieapp.viewmodel.MovieViewModel
import jp.wasabeef.blurry.Blurry

class DetailMovie : AppCompatActivity() {
    companion object{
        const val MOVIE_ID = "idMovie"
    }
    private lateinit var binding: ActivityDetailMovieBinding
    private lateinit var movieViewModel: MovieViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailMovieBinding.inflate(layoutInflater)
        setSupportActionBar(binding.toolbar)
        setContentView(binding.root)
        movieViewModel = ViewModelProvider(this, ViewModelProvider.NewInstanceFactory()).get(MovieViewModel::class.java)
        val id = intent.getIntExtra(MOVIE_ID, 0)
        binding.collapsingDetail.title = getString(R.string.titleBar)
        showLoading(true)
        binding.collapsingDetail.setCollapsedTitleTextColor(
            ContextCompat.getColor(this, R.color.black)
        )

        binding.collapsingDetail.setExpandedTitleColor(
            ContextCompat.getColor(this, android.R.color.transparent)
        )
        movieViewModel.detailMovie(id)
        movieViewModel.detailMovie.observe(this,{ data ->
            if (data == null){
                Toast.makeText(this, "Something Wrong", Toast.LENGTH_SHORT).show()
            } else {
                showLoading(false)
                initView(data)
            }
        })

    }

    private fun initView(detail: MovieDetailItem){

        val baseImageUrl = BuildConfig.BASE_IMAGE
        val urlImage = "$baseImageUrl${detail.poster_path}"
        val backdrop = "$baseImageUrl${detail.backdrop_path}"
        val rate = detail.vote_average?.div(2)
        binding.mvTitleDetail.text = detail.title
        binding.mvOverview.text = detail.overview
        binding.mvRate.text = detail.vote_average.toString()
        binding.mvRelease.text = detail.release_date
        binding.mvRateDetail.rating = rate?.toFloat() ?: 0f
        binding.tvDuration.text = "${detail.runtime.toString()} Minutes"
        binding.tvVoteCount.text = detail.vote_count.toString()
        binding.tvPopularity.text = detail.popularity.toString()
        Glide.with(applicationContext).load(urlImage).into(binding.mvImagedetail)
        Glide.with(applicationContext).load(backdrop).into(binding.mvImageBackdropdetail)

    }

    private fun showLoading(isLoading: Boolean){
        if(isLoading){
            binding.pbMovieDetail.visibility = View.VISIBLE
        } else {
            binding.pbMovieDetail.visibility = View.GONE
        }
    }
}