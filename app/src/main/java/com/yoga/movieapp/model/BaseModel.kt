package com.yoga.movieapp.model

data class BaseModel<T>(
    var page: Int?,
    var results: T
)