package com.yoga.movieapp.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yoga.movieapp.BuildConfig
import com.yoga.movieapp.R
import com.yoga.movieapp.model.Results
import com.yoga.movieapp.ui.DetailMovie
import kotlinx.android.synthetic.main.activity_detail_movie.view.*
import kotlinx.android.synthetic.main.item_movie.view.*
import kotlinx.android.synthetic.main.item_movie.view.mvRate

class MovieAdapter(val context: Context, private val listItem: List<Results>): RecyclerView.Adapter<MovieHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieHolder {
        val vh = LayoutInflater.from(parent.context).inflate(R.layout.item_movie, parent, false)
        return MovieHolder(vh)
    }

    override fun onBindViewHolder(holder: MovieHolder, position: Int) {
        holder.setupView(listItem[position])
    }

    override fun getItemCount(): Int = listItem.size
}

class MovieHolder(itemView: View): RecyclerView.ViewHolder(itemView){
    fun setupView(item: Results){
        val rating = item.vote_average?.div(2)
        val baseImageUrl = BuildConfig.BASE_IMAGE
        val urlImage = "$baseImageUrl${item.poster_path}"
        itemView.mvTitle.text = item.title
        itemView.mvRate.text = item.vote_average.toString()
        itemView.tvOverview.text = item.overview
        itemView.mvRateStar.rating = rating?.toFloat() ?: 0f

        Glide.with(itemView.context).load(urlImage).into(itemView.mvImage)

        itemView.setOnClickListener {
            val i = Intent(it.context, DetailMovie::class.java).apply {
                putExtra(DetailMovie.MOVIE_ID, item.id)
            }
            it.context.startActivity(i)
        }
    }
}